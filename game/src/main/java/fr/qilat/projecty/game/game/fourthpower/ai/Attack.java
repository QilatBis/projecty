package fr.qilat.projecty.game.game.fourthpower.ai;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Attack {

    @Getter
    private List<Scheme> attacks;

    public Attack(List<Scheme> attacks) {
        this.attacks = attacks;
    }

    public List<Scheme> aboutToWin() {
        List<Scheme> finalSchemes = new ArrayList<>();
        for (Scheme attack : attacks) {
            if (attack.getNbOfElementsFilled() == attack.getMaxNbOfElement() - 1) {
                finalSchemes.add(attack);
            }
        }
        return finalSchemes;
    }

    public List<Scheme> getMostPromisingAttack() {
        List<Scheme> schemes = new ArrayList<>();
        int maxAmountFound = 0;
        for (Scheme attack : attacks) {
            int nbOFElementsFilled = attack.getNbOfElementsFilled();
            if (nbOFElementsFilled > maxAmountFound) {
                schemes = new ArrayList<>();
                schemes.add(attack);
            } else if (nbOFElementsFilled == maxAmountFound) {
                schemes.add(attack);
            }
        }
        return schemes;
    }

}
