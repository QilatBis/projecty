package fr.qilat.projecty.game.game.fourthpower.mainmenu;

import fr.qilat.projecty.game.engine.IHud;
import fr.qilat.projecty.game.engine.Window;
import fr.qilat.projecty.game.engine.graph.*;
import fr.qilat.projecty.game.engine.items.ButtonItem;
import fr.qilat.projecty.game.engine.items.GameItem;
import fr.qilat.projecty.game.engine.items.TextItem;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class MainMenuHud implements IHud {

    private static final Font FONT = new Font("Arial", Font.PLAIN, 20);
    private static final String CHARSET = "ISO-8859-1";

    public static final String BACKGROUND = "/textures/mainmenu/background.png";

    private GameItem[] gameItems;


    private ArrayList<ButtonItem> buttonItems;

    private ButtonItem startOnline;
    private ButtonItem startVsIA;
    private ButtonItem settings;
    private ButtonItem quit;
    private final TextItem statusTextItem;
    private GameItem background;


    public MainMenuHud() throws Exception {
        background = new GameItem();
        Texture texture = new Texture(BACKGROUND);
        Material material = new Material(texture);
        Mesh mesh = OBJLoader.loadMesh("/models/background.obj");
        material.setAmbientColour(new Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
        mesh.setMaterial(material);
        background.setMesh(mesh);
        background.setScale(1f);
        background.setPosition(0f, 0f, 0f);

        FontTexture fontTexture = new FontTexture(FONT, CHARSET);

        this.statusTextItem = new TextItem("Hello World", fontTexture);
        this.statusTextItem.getMesh().getMaterial().setAmbientColour(new Vector4f(1, 1, 1, 1));


        buttonItems = new ArrayList<>();
        //FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        startOnline = new ButtonItem(new Vector3f(20, 20, 20), "Play Online", fontTexture);
        buttonItems.add(startOnline);
        /*startVsIA = new ButtonItem("Play vs IA", fontTexture);
        buttonItems.add(startVsIA);
        settings = new ButtonItem("Paramètres", fontTexture);
        buttonItems.add(settings);
        quit = new ButtonItem("Quitter", fontTexture);
        buttonItems.add(quit);*/

        this.gameItems = new GameItem[]{statusTextItem, background, startOnline};
    }

    public void handleClick(double x, double y) {
        System.out.println(x + " / " + y);
        for (ButtonItem buttonItem : this.buttonItems) {
            System.out.println(buttonItem.getText());
            if (buttonItem.isClicked(x, y)) {
                System.out.println(buttonItem.getText());
                buttonItem.onClick();
            }
        }
    }

    @Override
    public GameItem[] getGameItems() {
        return gameItems;
    }

    public void updateSize(Window window) {
        this.background.setPosition(10f, window.getHeight() - 50f, -5f);
        this.statusTextItem.setPosition(10f, window.getHeight() - 50f, 0);
    }

}
