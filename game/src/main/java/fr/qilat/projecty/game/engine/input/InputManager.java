package fr.qilat.projecty.game.engine.input;

import org.lwjgl.glfw.GLFWKeyCallback;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class InputManager extends GLFWKeyCallback {

    private static InputManager manager;
    private List<Object> inputListener;
    private boolean[] keys;

    private InputManager() {
        this.inputListener = new ArrayList<>();
        this.keys = new boolean[65536];
    }

    public static InputManager get() {
        if (manager == null) manager = new InputManager();
        return manager;
    }

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        keys[key] = (action != GLFW_RELEASE);
        if (keys[key]) {
            this.inputProcess(key);
        }
    }

    public boolean isKeyDown(int keycode) {
        return keys[keycode];
    }


    public void registerListener(Object object) {
        this.inputListener.add(object);
    }

    public void unregisterListener(Object object) {
        this.inputListener.remove(object);
    }

    public void inputProcess(int keyCode) {
        inputListener.forEach(o -> {
            for (Method method : o.getClass().getMethods()) {
                if (method.isAnnotationPresent(InputListener.class)) {
                    try {
                        method.invoke(o, keyCode);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
