package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.Scene;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Theophile on 2018-12-14 for projecty.
 */
public class GridContent {
    @Getter
    private int amountToWon;
    @Setter
    @Getter
    private Chip[][] chips;

    public GridContent(int columnsAmount, int rowsAmount, int amountToWon) throws Exception {
        this.chips = new Chip[columnsAmount][rowsAmount];
        this.amountToWon = amountToWon;
        if (Math.sqrt(Math.pow(columnsAmount, 2) * Math.pow(rowsAmount, 2)) < Math.pow(amountToWon, 2)) {
            throw new Exception("AmountToWon is too high !");
        }
    }

    public boolean addChipToColumn(Scene scene, int columnId, Player playingPlayer) {
        for (int i = 0; i < chips[0].length; i++) {
            if (chips[columnId][i] == null) {
                chips[columnId][i] = new Chip(playingPlayer, columnId, i);
                scene.addGameItem(chips[columnId][i]);
                return true;
            }
        }
        return false;
    }

    public Player hasSomeoneWon() {
        Chip winner;
        for (int i = 0; i < chips.length; i++) {
            for (int j = 0; j < chips[0].length; j++) {
                if (chips[i][j] != null) {
                    if (i < (chips.length - amountToWon)) {
                        if ((winner = checkHorizontal(i, j)) != null) {
                            return winner.getPlayer();
                        }
                        if (j < (chips[0].length - amountToWon)) {
                            if ((winner = checkAscendantDiag(i, j)) != null) {
                                return winner.getPlayer();
                            }
                        }
                        if (j >= amountToWon - 1) {
                            if ((winner = checkDescendantDiag(i, j)) != null) {
                                return winner.getPlayer();
                            }
                        }
                    }
                    if (j < (chips[0].length - amountToWon)) {
                        if ((winner = checkVertical(i, j)) != null) {
                            return winner.getPlayer();
                        }
                    }
                }
            }
        }
        return null;
    }

    private Chip checkHorizontal(int column, int row) {
        Chip firstChip = chips[column][row];
        for (int i = 0; i < amountToWon; i++)
            if (chips[column + i][row] == null
                    || !chips[column + i][row].equals(firstChip)) return null;
        return firstChip;
    }

    private Chip checkVertical(int column, int row) {
        Chip firstChip = chips[column][row];
        for (int i = row + 1; i < (row + amountToWon); i++)
            if (chips[column][i] == null
                    || !chips[column][i].equals(firstChip)) return null;
        return firstChip;
    }

    private Chip checkAscendantDiag(int column, int row) {
        Chip firstChip = chips[column][row];
        for (int i = 1; i < amountToWon; i++)
            if (chips[column + i][row + i] == null
                    || !chips[column + i][row + i].equals(firstChip))
                return null;
        return firstChip;
    }

    private Chip checkDescendantDiag(int column, int row) {
        Chip firstChip = chips[column][row];
        for (int i = 1; i < amountToWon; i++) {
            if (chips[column + i][row - i] == null
                    || !chips[column + i][row - i].equals(firstChip)) {
                return null;
            }
        }
        return firstChip;
    }
}
