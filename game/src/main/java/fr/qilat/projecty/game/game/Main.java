package fr.qilat.projecty.game.game;

import fr.qilat.projecty.game.engine.GameEngine;
import fr.qilat.projecty.game.engine.IGameLogic;
import fr.qilat.projecty.game.game.fourthpower.mainmenu.MainMenu;

public class Main {

    public static void main(String[] args) {

        try {
            boolean vSync = true;
            IGameLogic gameLogic = new MainMenu();
            GameEngine gameEng = new GameEngine("Puissance 4", 1080, 920, vSync, gameLogic);
            gameEng.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
