package fr.qilat.projecty.game.engine.items;

import fr.qilat.projecty.game.engine.graph.Material;
import fr.qilat.projecty.game.engine.graph.Mesh;
import fr.qilat.projecty.game.engine.graph.OBJLoader;
import fr.qilat.projecty.game.engine.graph.Texture;

/**
 * Created by Theophile on 2018-12-12 for projecty.
 */
public class SkyBox extends GameItem {

    public SkyBox(String objModel, String textureFile) throws Exception {
        super();
        Mesh skyBoxMesh = OBJLoader.loadMesh(objModel);
        Texture skyBoxtexture = new Texture(textureFile);
        skyBoxMesh.setMaterial(new Material(skyBoxtexture, 0.0f));
        setMesh(skyBoxMesh);
        setPosition(0, 0, 0);
    }
}