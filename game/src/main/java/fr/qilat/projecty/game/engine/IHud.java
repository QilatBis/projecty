package fr.qilat.projecty.game.engine;

import fr.qilat.projecty.game.engine.items.GameItem;

/**
 * Created by Theophile on 2018-12-11 for ProjectY.
 */
public interface IHud {

    GameItem[] getGameItems();

    default void cleanup() {
        GameItem[] gameItems = getGameItems();
        for (GameItem gameItem : gameItems) {
            gameItem.getMesh().cleanUp();
        }
    }
}