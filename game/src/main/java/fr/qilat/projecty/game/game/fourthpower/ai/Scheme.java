package fr.qilat.projecty.game.game.fourthpower.ai;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Scheme {

    @Getter
    private int maxNbOfElement;

    @Getter
    private HashMap<Coords, Boolean> filledState;

    public Scheme(int maxNbOfElement) {
        this.maxNbOfElement = maxNbOfElement;
        this.filledState = new HashMap<>();
    }

    public void addState(Coords coords, boolean filled) {
        this.filledState.put(coords, filled);
    }

    public boolean containsState(Coords coords) {
        return this.filledState.containsKey(coords);
    }

    public int getNbOfElementsFilled() {
        int amount = 0;
        for (boolean filled : filledState.values())
            if (filled) amount++;
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scheme scheme = (Scheme) o;

        for (Coords coords : scheme.filledState.keySet()) {
            if (!this.getFilledState().containsKey(coords))
                return false;
        }

        return true;
    }

    public List<Coords> getPlayableCoords() {
        List<Coords> playableCoords = new ArrayList<>();
        for (Coords coords : filledState.keySet()) {
            if (!filledState.get(coords))
                playableCoords.add(coords);
        }
        return playableCoords;
    }
}
