package fr.qilat.projecty.game.game.fourthpower;

import fr.qilat.projecty.game.engine.*;
import fr.qilat.projecty.game.engine.graph.Camera;
import fr.qilat.projecty.game.engine.graph.Renderer;
import fr.qilat.projecty.game.engine.graph.lights.DirectionalLight;
import fr.qilat.projecty.game.engine.items.SkyBox;
import fr.qilat.projecty.game.game.fourthpower.match.Hud;
import fr.qilat.projecty.game.game.fourthpower.match.Match;
import fr.qilat.projecty.game.game.fourthpower.match.Player;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Theophile on 2018-12-14 for projecty.
 */
public class FourthPower implements IGameLogic {
    private static final float CAMERA_POS_STEP = 0.10f;

    private static final float MOUSE_SENSITIVITY = 0.2f;

    private final Vector3f cameraInc;

    private final Renderer renderer;

    private final Camera camera;

    private Player player1;
    private Player player2;

    private Scene scene;
    private Match match;

    private Hud hud;

    public FourthPower() {
        cameraInc = new Vector3f(0.0f, 0.0f, 0.0f);
        renderer = new Renderer();
        camera = new Camera(new Vector3f(8.89f, 18.39f, 63.28f), new Vector3f(2.19f, -0.71f, 0));
    }

    private void setupLight() {
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);
        sceneLight.setAmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));
        sceneLight.setSkyBoxLight(new Vector3f(1.0f, 1.0f, 1.0f));
        float lightIntensity = 1.0f;
        Vector3f lightPosition = new Vector3f(1, 1, 0);
        sceneLight.setDirectionalLight(new DirectionalLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity));
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        Player player1 = new Player("Joueur 1", new Vector4f(1.0f, 0.5f, 0.0f, 1.0f));
        Player player2 = new Player("Joueur 2", new Vector4f(0.0f, 1.0f, 0.0f, 1.0f));
        this.player1 = player1;
        this.player2 = player2;
        //this.player2.setAI(true);

        scene = new Scene();
        match = new Match(this.player1, this.player2, scene);
        this.match.getSelector().resetPosition();

        scene.addGameItem(this.match.getGrid());
        scene.addGameItem(this.match.getSelector());

        // Setup  SkyBox
        float skyBoxScale = 100.0f;
        SkyBox skyBox = new SkyBox("/models/skybox.obj", "/textures/skybox.png");
        skyBox.setScale(skyBoxScale);
        scene.setSkyBox(skyBox);

        setupLight();

        hud = new Hud("Puissance 4");
    }

    @Override
    public void input(Window window, MouseInput mouseInput) throws Exception {
        cameraInc.set(0, 0, 0);
        if (window.isKeyPressed(GLFW_KEY_W)) {
            cameraInc.z = -2;
        } else if (window.isKeyPressed(GLFW_KEY_S)) {
            cameraInc.z = 2;
        }
        if (window.isKeyPressed(GLFW_KEY_A)) {
            cameraInc.x = -2;
        } else if (window.isKeyPressed(GLFW_KEY_D)) {
            cameraInc.x = 2;
        }
        if (window.isKeyPressed(GLFW_KEY_LEFT_SHIFT)) {
            cameraInc.y = -2;
        } else if (window.isKeyPressed(GLFW_KEY_SPACE)) {
            cameraInc.y = 2;
        }

        if (window.isKeyPressed(GLFW_KEY_SPACE)) {
            this.match = new Match(player1, player2, this.scene);
            this.scene.cleanup();
            this.scene.getGameMeshes().clear();
            scene.addGameItem(this.match.getGrid());
            scene.addGameItem(this.match.getSelector());
        }
    }

    @Override
    public void update(float interval, MouseInput mouseInput) {
        if (mouseInput.isRightButtonPressed()) {
            Vector2f rotVec = mouseInput.getDisplVec();
            camera.moveRotation(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }
        camera.movePosition(cameraInc.x * CAMERA_POS_STEP, cameraInc.y * CAMERA_POS_STEP, cameraInc.z * CAMERA_POS_STEP);

        this.match.getGrid().updateChipsPositions(interval);


        Player winner = this.match.getGrid().getContent().hasSomeoneWon();
        this.hud.setStatusText(winner != null ? winner.getName() + "has won ! " : "");


    }

    @Override
    public void render(Window window) {
        hud.updateSize(window);
        renderer.render(window, camera, scene, hud);
    }

    @Override
    public void cleanup() {
        this.renderer.cleanup();

        if (this.scene != null)
            this.scene.cleanup();
        if (this.hud != null)
            this.hud.cleanup();
    }
}
