package fr.qilat.projecty.game.game.fourthpower.match;

import fr.qilat.projecty.game.engine.graph.Material;
import fr.qilat.projecty.game.engine.graph.Mesh;
import fr.qilat.projecty.game.engine.graph.OBJLoader;
import fr.qilat.projecty.game.engine.items.GameItem;
import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4f;

/**
 * Created by Theophile on 2018-12-15 for projecty.
 */
public class Selector extends GameItem {

    private static final String MODEL_PATH = "/models/selector.obj";
    private static final float[] COORDS = new float[]{-6.3f, 37f, 0f};
    private static final float DELTA = 5.5f;

    @Getter
    private int maxPosition;
    @Getter
    private int selectedPosition;
    @Getter
    private Vector4f color;
    @Getter
    @Setter
    private boolean visible;

    public Selector(Vector4f color, int maxPosition) throws Exception {
        this.maxPosition = maxPosition;
        this.selectedPosition = 0;
        this.color = color;
        this.visible = true;
        this.setScale(3f);

        Mesh mesh = OBJLoader.loadMesh(MODEL_PATH);
        Material material = new Material(this.color, 1.0f);
        mesh.setMaterial(material);
        this.setMesh(mesh);
        this.updateLocation();
    }

    public void setColor(Vector4f color) {
        this.color = color;
        this.getMesh().setMaterial(new Material(color, 1.0f));
    }

    public void resetPosition() {
        this.selectedPosition = 0;
        this.updateLocation();
    }

    public boolean incrementPosition() {
        this.selectedPosition++;
        if (this.selectedPosition > maxPosition - 1) {
            this.selectedPosition = this.maxPosition - 1;
            this.updateLocation();
            return false;
        }
        this.updateLocation();
        return true;
    }

    public boolean decrementPosition() {
        this.selectedPosition--;
        if (this.selectedPosition < 0) {
            this.selectedPosition = 0;
            this.updateLocation();
            return false;
        }
        this.updateLocation();
        return true;
    }

    public void updateLocation() {
        this.setPosition(COORDS[0] + (DELTA * selectedPosition), COORDS[1], COORDS[2]);
    }

}
